<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

/*Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');*/

/*Rutas API*/

$api->version('v1', function ($api) {
 
	$api->group(['namespace' => 'App\Http\Controllers'],function($api)
	{

		 $api->get('/home', 'Fotos@mostrar');
		 $api->get('/', 'HomeController@index');
		 $api->get('/galeria', 'HomeController@fotos');
		 $api->get('/habitacion', 'HomeController@habitacion');
		 $api->get('/ubicacion', 'HomeController@ubicacion');
		 $api->resource('/fotos', 'Fotos');
	});

});