<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/home', 'Fotos@mostrar');


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/galeria', 'HomeController@fotos');
Route::get('/habitacion', 'HomeController@habitacion');
Route::get('/ubicacion', 'HomeController@ubicacion');


//Fotos

Route::resource('/fotos', 'Fotos');