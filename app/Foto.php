<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    //
    protected $fillable = [
        'titulo', 'descripcion', 'urlImg',
    ];
}
