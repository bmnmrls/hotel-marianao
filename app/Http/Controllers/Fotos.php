<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Foto;
use Storage;
// Utilidad para las fechas.
use Carbon\Carbon;

class Fotos extends Controller
{

        public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
    }

     public function mostrar()

    {
        $fotos = Foto::all();
        return view('home')->with(['fotos' => $fotos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
          $this->validate($request, [
            'titulo' => 'required',
            'descripcion' => 'required',
            //'urlImg'=>'required|image|max:20000',
            ]);

          $foto = new Foto();
          $foto->titulo = $request->titulo;
          $foto->descripcion = $request->descripcion;  

          //para guardar fotos

          $img = $request->file('urlImg');
          $ruta='/imgFotos/';
          //$file_route = time().'_'.$img->guessExtension();
          $file_route = time().'_'.$img->guessExtension();
          //Storage::disk('imgFotos')->put($file_route, file_get_contents( $img->getRealPath() ));
          $img->move(getcwd().$ruta,$file_route);

          $foto->urlImg = $file_route;
          
          if($foto->save()){

            return back()->with('msj', 'La foto ha sido guardada');

          } else {
            return back()->with('errormsj', 'Error al guardar la imagen');
          }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $foto = Foto::find($id);
        return view('home')->with(['edit' => true, 'foto' => $foto]);

    }


    public function update(Request $request, $id)
    {
            $this->validate($request, [
            'titulo' => 'required',
            'descripcion' => 'required',
            //'urlImg'=>'required|image|max:20000',
            ]);

          $foto = Foto::find($id);
          $foto->titulo = $request->titulo;
          $foto->descripcion = $request->descripcion;  

          //para guardar fotos

          $img = $request->file('urlImg');
          $file_route = time().'_'.$img->getClientOriginalName();
          Storage::disk('imgFotos')->put($file_route, file_get_contents( $img->getRealPath() ));
          Storage::disk('imgFotos')->delete($request->img);

          $foto->urlImg = $file_route;
          
          if($foto->save()){

            return redirect('home')->with('msj', 'La foto ha sido guardada');
            
          } else {
            return back()->with('errormsj', 'Error al guardar la imagen');
          }
    }


    public function destroy($id)
    {
        Foto::destroy($id);
        return back();
    }
}
