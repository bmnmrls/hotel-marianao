<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Foto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = Foto::all();
        return view('welcome');
    }

        public function fotos()
    {
        $fotos = Foto::all();
        return view('usuario/galeria')->with(['fotos' => $fotos]);
    }

    public function habitacion()
    {
        return view('usuario/habitacion');
    }

        public function ubicacion()
    {
        return view('usuario/ubicacion');
    }
}
