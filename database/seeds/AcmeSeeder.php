<?php
	
use Illuminate\Database\Seeder;  
  class AcmeSeeder extends Seeder
  {
    public function run()
    {
      if (!isset($this->table))
      {
        throw new Exception("Ninguna tabla especificada");
      }

      if (method_exists(get_class(),'getData'))
      {
        throw new Exception("Ningun dato especificado");
        
      }

      DB::table($this->table)->truncate();
      DB::table($this->table)->insert($this->getData());
    }      
        
  }