function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 20.2084186, lng: -96.7735943},
    zoom: 17
  });

  var infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);

  service.getDetails({
    placeId: 'ChIJ9-T9rnOg24UR5OdDTZd9Yj4'
  }, function(place, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }
  });
}