<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<title>Hotel Marianao</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
		<!-- CSS Materialize-->
	<link rel="stylesheet" href="{{ asset('plugins/materialize/css/materialize.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Bitter|Cedarville+Cursive|Zeyada" rel="stylesheet">
  <!-- Mis Estilos-->
  <link rel="stylesheet" href="{{ asset('plugins/materialize/css/estilos.css') }}">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>


	<div class="navbar-fixed">
	  <nav class="white" role="navigation">
	      <div class="nav-wrapper">

	        <ul class="left hide-on-med-and-down">
	          <li><a href="{{ url('/') }}">Inicio</a></li>
	          <li><a href="">Habitaciones</a></li>
	          <li><a href="{{ url('/galeria/galeria')}}">Galeria</a></li>
	          <li><a href="">Ubicación</a></li>
	          <li><a href="{{ url('/login') }}">LogIn</a></li>
	        </ul>

	        <ul class="hide-on-med-and-down center">
	          <li class="centro"><h3 class="marianao">Hotel Marianao</h3></li>
	        </ul>

	        <ul class="right hide-on-med-and-down">
	          <li class="fondo"><a href="" class="fondo"><i class="material-icons iconos left">call</i>01 235 328 0064</a></li>
	        </ul>
	     
	        <ul id="nav-mobile" class="side-nav">
	          <li><a href="index.html">INICIO</a></li>
	          <li><a href="habitaciones.html">HABITACIONES</a></li>
	          <li><a href="habitaciones.html">GALERIA</a></li>
	          <li><a href="habitaciones.html">UBICACIÓN Y CONTACTO</a></li>
	        </ul>
	        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
	        </div>
	  </nav>
	</div>

	@yield('content')

	 <!-- Jquery-->
	  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	  <!-- Compiled and minified JavaScript -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	  <!-- Inicializando Carrusel-->
	  <script type="text/javascript">
	  $(document).ready(function(){
	      $('.slider').slider({full_width: true});
	      $('.button-collapse').sideNav();
	  });

	  $(document).ready(function(){
	      $('.parallax').parallax();
	    });
	  </script>
</body>
</html>
