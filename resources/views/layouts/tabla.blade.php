<table class="responsive-table highlight">
	@if(isset($fotos))
	<thead>
		<th>Titulo</th>
		<th>Descripción</th>
		<th>Imagen</th>
		<th></th>
	</thead>

	<tbody>
		@foreach($fotos as $n)
			<tr>
				<td>{{ $n->titulo }}</td>
				<td>{{ $n->descripcion }}</td>
				<td>
					<img class="responsive-img" width="100" src="imgFotos/{{$n->urlImg}}">
				</td>
				<td>
					<a href="fotos/{{$n->id}}/edit" class="btn btn-xs blue">Modificar</a>

					<form action="{{route('fotos.destroy',$n->id)}}" method="POST" >
						<input name="_method" type="hidden" value="DELETE">
						   {{ csrf_field() }}
						<input type="submit" class="btn btn-xs red" value="Eliminar">
					</form>
				</td>				
			</tr>
		@endforeach
	</tbody>
	@endif
</table>