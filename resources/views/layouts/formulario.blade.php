@if(session()->has('msj'))
<div class="alert alert-success" role="alert">
  {{session('msj')}}
</div>
@endif
@if(session()->has('errormsj'))
<div class="alert alert-danger">
  {{session('errormsj')}}
</div>
@endif
<form method="POST" role="form" action="{{url('fotos')}}" enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input type="text" name="titulo" class="validate">
          <label for="titulo">Título</label>
          @if($errors->has('titulo'))
           <span style="color:red;">{{$errors->first('titulo')}}</span>
          @endif
        </div>
      </div>

      <div class="row">
        <div class="input-field col s12">
          <textarea name="descripcion" class="materialize-textarea"></textarea>
          <label for="descripcion">Descripción</label>

           @if($errors->has('descripcion'))
             <span style="color:red;">{{$errors->first('descripcion')}}
             </span>
          @endif

        </div>
      </div>

      <form action="#">
        <div class="file-field input-field">
          <div class="btn">
            <span>Imagen</span>
            <input name="urlImg" type="file">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" name="urlImg">
          </div>
        </div>
      </form>
    </form>
      <div class="col s6"> 
      <br>
       <button class="btn waves-effect waves-light  " type="submit" name="action">Crear
       </button>
      </div>
  </div>
</form> 