 @if(session()->has('msj'))
<div class="alert alert-success" role="alert">
  {{session('msj')}}
</div>
@endif
@if(session()->has('errormsj'))
<div class="alert alert-danger">
  {{session('errormsj')}}
</div>
@endif

@if(isset($foto))
<form method="POST" role="form" action="{{ route('fotos.update', $foto->id) }}" enctype="multipart/form-data">
<input name="_method" type="hidden" value="PUT">
<input class="hide" type="text" name="img" value="{{$foto->urlImg}}">


   {{ csrf_field() }}
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input type="text" name="titulo" class="validate" value="{{$foto->titulo}}">
          <label for="titulo">Título</label>
          @if($errors->has('titulo'))
           <span style="color:red;">{{$errors->first('titulo')}}</span>
          @endif
        </div>
      </div>

      <div class="row">
        <div class="input-field col s12">
        <label for="descripcion">Descripción</label>
          <textarea name="descripcion" class="materialize-textarea"> {{$foto->descripcion}}</textarea>
          

           @if($errors->has('descripcion'))
             <span style="color:red;">{{$errors->first('descripcion')}}
             </span>
          @endif

        </div>
      </div>

      <form action="#">
        <div class="file-field input-field">
          <div class="btn">
            <span>Imagen</span>
            <input name="urlImg" type="file" >
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" name="urlImg">
          </div>
        </div>
      </form>
    </form>
      <div class="col s6"> 
      <br>
       <button class="btn waves-effect waves-light red" type="submit" name="action">Modificar
       </button>
      </div>
  </div>
</form> 
@endif