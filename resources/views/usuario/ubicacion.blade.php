@extends('app')




@section('content')

<br><br>
  
  <div class="row">
    <div class="col s4">
      <div id="map" class="tamanoMapa"></div>  
    </div>
    <div class="col s6 precio">
    		<br><br>
          Ubicado muy cerca de la Costa Esmeralda cuenta con amplias y confortables habitaciones con aire acondicionado, ventilador de techo, televisión por cable, internet inalámbrico y agua caliente día y noche.
    </div>
  </div>

@include('partials.footer')
@endsection