@extends('app')

@section('content')
    
  <div class="flex-center position-ref full-height">   <div class="container">
  <br>
        <div class="row">
           @if(isset($fotos))
             @foreach($fotos as $n)
               <div class="col s4 img">
                               
                <img class="materialboxed responsive-img" data-caption="{{$n->descripcion}}}" src="imgFotos/{{$n->urlImg}}">
                <br>
       
               </div>
             @endforeach
           @endif
                        
          </div>
        </div>
       </div>
   </div>
  

  @include('partials.footer')
@endsection


