@extends('app')

@section('content')

<br>

<div class="container">
  <h2 class="center-align titulo">HABITACIONES</h2>
  <br> <br>
  <h4 class="norma center-align ">Nuestros precios son súper económicos y accesibles</h4>
  <br>
  <div class="row">
    <div class="demo col s6">
          <div class="item">            
              <div class="clearfix" style="max-width:474px;">
                  <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                      <li data-thumb="imgs/DSC00623.JPG"> 
                           <img src="imgs/DSC00623.JPG" class="responsive-img"> 
                           </li>
                      <li data-thumb="imgs/DSC00626.JPG"> 
                           <img src="imgs/DSC00626.JPG" class="responsive-img"> 
                           </li>
                  </ul>
              </div>
          </div>
      </div>
    <div class="col s6 precio">
      <br><br>
      Habitación doble            $450.00 <br>
      Persona adicional           $50.00
    </div>
  </div>
  <div class="reserva">
  Le solicitamos muy atentamente que un día antes, usted nos haga la reservación por teléfono para confirmarle nuestra disponibilidad confirmándonos su llegada y aquí estaremos esperándolo
</div>
</div>


@include('partials.footer')
@endsection

