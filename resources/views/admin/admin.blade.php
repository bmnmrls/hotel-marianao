<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
		<title>Hotel Marianao</title>

		<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
			<!-- CSS Materialize-->
		<link rel="stylesheet" href="{{ asset('plugins/materialize/css/materialize.css') }}">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Bitter|Cedarville+Cursive|Zeyada" rel="stylesheet">
	  <!-- Mis Estilos-->
	  <link rel="stylesheet" href="{{ asset('plugins/materialize/css/estilos.css') }}">

		<!-- Fonts -->
		<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	</head>
	<body>

		<div class="navbar-fixed">
		  <nav class="white" role="navigation">
		      <div class="nav-wrapper">

		        <ul class="left hide-on-med-and-down">
		          <li><a href="{{ url('/register') }}">Registrar</a></li>
		          <li><a href="{{ url('/home') }}">Editar Galeria</a></li>
		        </ul>

		        <ul class="hide-on-med-and-down center">
		          <li class="admin"><h3 class="marianao">Administración</h3></li>
		        </ul>

		        <ul class="right hide-on-med-and-down">
		           <li>
                      <a href="{{ url('/logout') }}" class="btn red" 
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                                            Salir
                       </a>

                       <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                       </form>
                    </li>
		        </ul>
		     
		        <ul id="nav-mobile" class="side-nav">
		          <li><a href="{{ url('/register') }}">Registrar</a></li>
		          		           <li>
                      <a href="{{ url('/logout') }}"  
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                                            Salir
                       </a>

                       <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                       </form>

                       <a href="{{ url('/home') }}">Editar Galeria</a>
                    </li>
		        </ul>
		        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
		        </div>
		  </nav>
		</div>

		@yield('content')

		 <!-- Jquery-->
		  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		  <!-- Compiled and minified JavaScript -->
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
		  <!-- Inicializando Carrusel-->
		  <script type="text/javascript">
		  $(document).ready(function(){
		      $('.slider').slider({full_width: true});
		      $('.button-collapse').sideNav();
		  });

		  $(document).ready(function(){
		      $('.parallax').parallax();
		    });
		  </script>
	</body>
</html>
