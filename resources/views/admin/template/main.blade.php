<!DOCTYPE html>
<html lang="es">
  
 @include('admin.template.partials.cabecera')

<body>
	@include('admin.template.partials.nav')

	<section>
		@yield('content')
	</section>
  <br><br><br><br><br>
  
  	<!-- Footer -->
  @include('admin.template.partials.footer')
 

  @include('admin.template.partials.scripts')  

 </body>
</html>