 <!-- Jquery-->
  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
  <!-- Inicializando Carrusel-->
  <script type="text/javascript">
  $(document).ready(function(){
      $('.slider').slider({full_width: true});
      $('.button-collapse').sideNav();
  });

  $(document).ready(function(){
      $('.parallax').parallax();
    });
  </script>