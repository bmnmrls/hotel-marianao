<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	 <!--  Android 5 Chrome Color-->
	<meta name="theme-color" content="#000">
	<title> @yield('title', 'Default') </title>
  	<!-- CSS Materialize-->
	<link rel="stylesheet" href="{{ asset('plugins/materialize/css/materialize.css') }}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Bitter|Cedarville+Cursive|Zeyada" rel="stylesheet">
  <!-- Mis Estilos-->
  <link rel="stylesheet" href="{{ asset('plugins/materialize/css/estilos.css') }}">
</head>