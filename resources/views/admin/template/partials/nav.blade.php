<div class="navbar-fixed">
  <nav class="white" role="navigation">
      <div class="nav-wrapper">

        <ul class="left hide-on-med-and-down">
          <li><a href="{{ url('/') }}">Inicio</a></li>
          <li><a href="">Habitaciones</a></li>
          <li><a href="">Galeria</a></li>
          <li><a href="">Ubicación</a></li>
          <li><a href="{{url('/login')}}">LogIn</a></li>
        </ul>

        <ul class="hide-on-med-and-down center">
          <li class="centro"><h3 class="marianao">Hotel Marianao</h3></li>
        </ul>

        <ul class="right hide-on-med-and-down">
          <li class="fondo"><a href="" class="fondo"><i class="material-icons iconos left">call</i>01 235 328 0064</a></li>
        </ul>
     
        <ul id="nav-mobile" class="side-nav">
          <li><a href="index.html">INICIO</a></li>
          <li><a href="habitaciones.html">HABITACIONES</a></li>
          <li><a href="habitaciones.html">GALERIA</a></li>
          <li><a href="habitaciones.html">UBICACIÓN Y CONTACTO</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
  </nav>
</div>