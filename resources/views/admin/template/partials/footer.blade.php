<footer class="page-footer black">
  <div class="container">
    <div class=" redes">
      <div class="container">
        <center>
          <div class="siguenos">¡Llámanos!</div>
              <h3 class="siguenos">Tel 01 235 328 0064</h3>
        </center>
      </div>
    </div>
  </div>
  <br>
  <div class="contacto container">
    <center>“En la contemplación de la naturaleza y en el silencio, busquemos la armónica belleza de la vida”. 
    <br>(Cora Cané (Periodista Argentina 1923-2016))</center>
  </div>
  <div class="footer-copyright container">
    <center>© Hotel Marianao</center>
  </div>
</footer>