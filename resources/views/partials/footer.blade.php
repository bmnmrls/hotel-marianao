<head>
  
</head>
<footer class="page-footer fondoFooter">
  <div class="container">
      
      <div class="llamanos posicion1">¡Llámanos!
        <div class="material-icons iconos left">01 235 328 0064</div>
      </div>
        
        
      <div class="llamanos posicion2">¡Síguenos!
        <div>  
          <a href="https://www.facebook.com/Hotel-Marianao-203910125452/"><img class="posicion" src="imgs/redes/f2.png"></a>   
        </div>
      </div>
        
 
  </div>
  <br>
  <div class="contacto container">
    <center>“En la contemplación de la naturaleza y en el silencio, busquemos la armónica belleza de la vida”
    <br>Cora Cané (Periodista Argentina 1923-2016)</center>
  </div>
  <div class="footer-copyright container">
    <center>© Hotel Marianao</center>
  </div>
</footer>