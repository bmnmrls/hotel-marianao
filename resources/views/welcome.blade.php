@extends('app')

@section('content')

<!--
Que el usuario NO esté validado y obtenga una excepción extraña
entonces se mostrará el mensaje de "error" en esta vista.
 -->
@if (Session::has('error')) 
<div class="alert alert-danger">
    <strong>Whoops!</strong> Ha surgido un problema.<br><br>
    {{Session::get('error')}}
</div>
@endif
    
     <!--Carrusel-->
  <div class="col s12">
  
    <div class="slider container container-radius">
      <ul class="slides">
        <li>
          <img src={{asset('imgs/DSCN4853.JPG')}} class="img-responsive carrusel "> 
          <div class="caption">
            <h3><br></h3>
            <h5 class="light grey-text text-lighten-3"></h5>
          </div>
        </li>
        <li>
          <img src="imgs/hoteldesdepatio.jpg"  class="img-responsive carrusel"> 
          <div class="caption left-align">
            <h3 class="textcar"><br><br>Estilo colonial</h3>
            <h5 class="light grey-text text-lighten-3"></h5>
          </div>
        </li>
        <li>
          <img src="imgs/DSCN4858.JPG" class="img-responsive carrusel"> 
          <div class="caption center-align">
            <h3></h3>
            <h3 class="textcar">Hospitalidad</h3>
          </div>
        </li>
       <li>
          <img src="imgs/hotelmarianaomayo2016015.JPG" class="img-responsive carrusel"> 
          <div class="caption right-align">
            <h3 class="textcar">Diversión <br>en<br>familia</h3>
          </div>
        </li>
      </ul>
    </div>
  </div>
<hr><!--Rayita punteda-->
</div>


  <div class="section fondo-principal">
    <div class="row container">
      <h2 class="header center">Hotel Marianao</h2>
      <p class="grey-text text-darken-3 lighten-3 introduccion">El hotel conserva su fascinante estilo colonial, con un inmejorable servicio y atención personalizada, teniendo como único fin el proporcionarle a usted una cómoda y placentera estancia, para que se olvide completamente del stress.
      <br><br
      >
      Para su seguridad le ofrece el servicio de amplio estacionamiento y una hermosa piscina que cuenta con una cascada (durante ciertos lapsos de tiempo durante el día) que le brinda a usted un maravilloso y rico masaje, rodeada de una exuberante vegetación.  </p>
    </div>
  </div>

@include('partials.footer')

@endsection