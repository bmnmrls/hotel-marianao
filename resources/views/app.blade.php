<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<title>Hotel Marianao</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
		<!-- CSS Materialize-->
	<link rel="stylesheet" href="{{ asset('plugins/materialize/css/materialize.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Bitter|Cedarville+Cursive|Zeyada" rel="stylesheet">
  <!-- Mis Estilos-->
  	<link rel="stylesheet" href="{{ asset('plugins/materialize/css/estilos.css') }}">
  	<link rel="stylesheet" href="{{ asset('plugins/materialize/css/lightslider.css') }}">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>


</head>
<body class="color fondo-principal">

	<div class="navbar-fixed">
	  <nav class="fondoNav" role="navigation">
	      <div class="nav-wrapper">

	        <ul class="left hide-on-med-and-down">
	          <li><a href="{{ url('/') }}">Inicio</a></li>
	          <li><a href="{{ url('habitacion') }}">Habitaciones</a></li>
	          <li><a href="{{ url('galeria')}}">Galeria</a></li>
	          <li><a href="{{ url('ubicacion')}}">Ubicación</a></li>
	          <li><a href="{{url('/login')}}">LogIn</a></li>
	        </ul>

	        <ul class="">
	          <li class="centro"><h3 class="marianao">Hotel Marianao</h3></li>
	        </ul>

	        <ul class="right hide-on-med-and-down">
	          <li class="fondo"><a href="#" class="fondo"><i class="material-icons iconos left">call</i>01 235 328 0064</a></li>
	        </ul>
	     
	        <ul id="nav-mobile" class="side-nav user">
	          <li><a href="{{ url('/') }}">INICIO</a></li>
	          <li><a href="{{ url('habitacion') }}">HABITACIONES</a></li>
	          <li><a href="{{ url('galeria')}}">GALERIA</a></li>
	          <li><a href="{{ url('ubicacion')}}">UBICACIÓN</a></li>
	        </ul>
	        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
	        </div>
	  </nav>
	</div>

	@yield('content')

	 <!-- Jquery-->
	  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe11T1nA6jV8ikpKwc45xwU8jtnmcFa4M&callback&signed_in=true&libraries=places&callback=initMap" async defer></script>
       <script src="{{ asset('plugins/materialize/js/ubicacion.js') }}"></script>
	  <!-- Compiled and minified JavaScript -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	  <!-- Inicializando Carrusel-->
	  <script type="text/javascript">
	  $(document).ready(function(){
	      $('.slider').slider({full_width: true});
	      $('.button-collapse').sideNav();
	  });

	  $(document).ready(function(){
	      $('.parallax').parallax();
	    });
	  </script>

	<!--Galeria-->
	<script src="{{ asset('plugins/materialize/js/lightslider.js') }}"></script> 
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
    </script>
</body>
</html>
